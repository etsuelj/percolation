"""
Code for Percolation (W.Kinzel/G.Reents, Physics by Computer)

This code plots the average cluster mass <s> as a function of p for
different values of L. A graph of the critical concentration p_c as a
function of L is also plotted.

NOTE: This code takes a really long time to finish running. Results can
be accessed at https://drive.google.com/open?id=0B1hoee6yCUvuREpvU1lfVm1OTm8.
"""
from __future__ import division
import numpy as np
import matplotlib.pyplot as plt

__author__ = "John Lawrence Euste"


def clm(data):
    '''Create a label map'''
    
    N = len(data)
    label = np.zeros_like(data, dtype = int)

    #set label of those touching the edge to -1
    label[:,0] = data[:,0]*-1
    label[0,:] = data[0,:]*-1
    label[:,N-1] = data[:,N-1]*-1
    label[N-1,:] = data[N-1,:]*-1
    
    for i in xrange(1,N-1):
        for j in xrange(1,N-1):
            if data[i,j]: # occupied
                #compare with up and left
                if data[i-1, j] and not(data[i,j-1]): # up occupied
                    label[i,j] = label[i-1,j]
                elif data[i, j-1] and not(data[i-1, j]): # left occupied
                    label[i,j] = label[i, j-1]
                elif not(data[i,j-1]) and not(data[i-1, j]): # none occupied
                    label[i,j] = np.max(label) + 1
                elif data[i-1, j] and data[i, j-1]: # both occupied
                    if label[i-1,j] < label[i, j-1]:
                        label[i,j] = label[i-1,j] # choose smaller label
                        label[label == label[i, j-1]] = label[i-1,j]
                    elif label[i-1,j] > label[i, j-1]:
                        label[i,j] = label[i,j-1] # choose smaller label
                        label[label == label[i-1, j]] = label[i,j-1]
                    else:
                        label[i,j] = label[i, j-1] 
                if i == N-2:
                    if label[i+1, j] == -1: # if touching the edge
                        label[label == label[i,j]] = -1
                if j == N-2:
                    if label[i, j+1] == -1: # if touching the edge
                        label[label == label[i,j]] = -1
    label[label==-1] = 0 # touching the edge
    return label


def count_size(label_array):
    '''Count the number and size of percolating clusters'''
    
    unique_label,unique_size = np.unique(label_array[label_array!=0],return_counts=1)
    #unique_size counts the mass of each cluster    
    num_clusters = len(unique_label) # number of clusters
    return num_clusters,unique_size

def find_pc(L):
    print 'Determining critical concentration for L=%i'%L
    grid = lambda L,p: np.random.random([L,L])<p # generate clusters

    #iteration    
    def iterate(p):
        perc_grid = grid(L,p)    
        label_map = clm(perc_grid)
                
        num_clusters,sizes = count_size(label_map)
        if len(sizes)!=0:
            s = np.mean(sizes)
        else:
            s = 0.
        return s # number of particles in percolating cluster
    
    s_all = []
    p_range = np.linspace(0,1,100) 
    trials = 100 # more trials should be done to increase accuracy
    for p in p_range:
        s_point = [iterate(p) for _ in xrange(trials)]
        s = np.mean(s_point)
        s_all.append(s)
    
    plt.plot(p_range,s_all,'o-')
    plt.title('L=%i'%L)
    plt.suptitle('%i pts, %i trials'%(len(p_range),trials))
    plt.xlabel('$p$',size='x-large')
    plt.ylabel('$<s>$',size='x-large')
    plt.savefig('Percolation\L%i_new_%i_%i.png'%(L,trials,len(p_range)))
    plt.close()
    
    p_c = p_range[np.argmax(s_all)] # critical concentration
    return p_c

#determining relationship between p_c and L
L_range =  np.arange(20,100,5)
pc_L = []
for L in L_range:
    pc = find_pc(L)
    print 'p_c = %.3f for L=%i'%(pc,L)
    pc_L.append(pc)
plt.plot(L_range,pc_L,'o',clip_on=0)
plt.xlabel('$L$',size='x-large')
plt.ylabel('$p_c$',size='x-large')
plt.savefig('Percolation\Lvp_c.png',bbox_inches='tight')
plt.close()
#save data for p_c at different values of L
np.savetxt('Percolation\pc.csv',pc_L,delimiter=',',fmt='%s')